﻿using TMPro;
using UnityEngine;

namespace Manager
{
    public class ScoreManager : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI scoreText;
        private GameManager gameManager;

        private static ScoreManager instance;

        public static ScoreManager Instance
            => instance;
        public void Init(GameManager gameManager)
        {
            this.gameManager = gameManager;
            this.gameManager.OnRestarted += OnRestarted;
            HideScore(false);
            SetScore(0);
        }

        public void SetScore(int score)
        {
            scoreText.text = $"Score : {score}";
        }

        private void Awake()
        {
            Debug.Assert(scoreText != null, "scoreText cannot null");
            instance = new ScoreManager();
        }

        private void OnRestarted()
        {
            gameManager.OnRestarted -= OnRestarted;
            HideScore(true);
            SetScore(0);
        }

        private void HideScore(bool hide)
        {
            scoreText.gameObject.SetActive(!hide);
        }
    }

}


