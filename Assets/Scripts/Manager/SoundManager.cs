﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Manager
{
    public class SoundManager : MonoBehaviour
    {

        // [SerializeField] private SoundClip[] soundClips;
        // [SerializeField] private AudioSource audioSource;
        //
        //
        // public static SoundManager Instance { get; private set; }
        //
        // [System.Serializable]
        // public struct SoundClip
        // {
        //     public Sound Sound;
        //     public AudioClip AudioClip;
        // }
        //
        // public enum Sound
        // {
        //     BGM,
        //     Fire,
        // }
        //
        // public void Play(AudioSource audioSource, Sound sound)
        // {
        //     Debug.Assert(audioSource != null, "audioSource cannot be null");
        //     audioSource.clip = GetAudioClip(sound);
        //     audioSource.Play();
        // }
        //
        // public void PlayBGM()
        // {
        //
        //     audioSource.loop = true;
        //     Play(audioSource, Sound.BGM);
        // }
        //
        // private AudioClip GetAudioClip(Sound sound)
        // {
        //     foreach (var soundClip in soundClips)
        //     {
        //         if (soundClip.Sound == sound)
        //         {
        //             return soundClips.AudioClip;
        //         }
        //     }
        //
        //     Debug.Assert(false, $"Cannot find sound.{sound}");
        //     return null;
        // }
        //
        // private void Awake()
        // {
        //     Debug.Assert(audioSource != null, "aaudioSource cannot be null");
        //     Debug.Assert(soundClips != null && soundClips.Length != 0, "sound clip need to be setup");
        //
        //     if (Instance == null)
        //     {
        //         Instance = this;
        //     }
        //
        //     DontDestroyOnLoad(this);
        // }
    }

}